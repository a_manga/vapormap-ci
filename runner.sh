docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull registry.gitlab.com/a_manga/vapormap-ci/mariadb:10.6.4
docker pull registry.gitlab.com/a_manga/vapormap-ci/backend:c504ba87
docker pull registry.gitlab.com/a_manga/vapormap-ci/frontend:c504ba87
docker-compose --env-file .venv -f docker-compose.yml up